
import {ADD_MOVIES,ADD_FAVOURITE, ADD_TO_FAVOURITE} from "../action/index"

var initialstate={
    list:[],
    favourites:[],
    val:"false"
}

export default function movies( state=initialstate,action){
    // if(action.type==ADD_MOVIES){
    //     return {
    //         ...state,
    //         list:action.movies
    //     }
    // }
    // return state;
    switch(action.type){
        case ADD_MOVIES:
            return{
                ...state,
                list:action.movies
            }
        case ADD_FAVOURITE:
            return{
                ...state,
                favourites:[action.movie,...state.favourites]
            }    
        case ADD_TO_FAVOURITE:
            return{
                ...state,
                val:action.val
            }
        default:
            return state;    
    }

}

