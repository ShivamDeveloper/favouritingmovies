import { render } from "@testing-library/react";
import React from "react"
import {data} from "../data"

  class Navbar extends React.Component {
    render(){

            return(
               <div className="nav">

                   <div className="search-container">
                       <input />
                       <button className="search-btn">Search</button>

                   </div>

               </div>
            );
    }

}

export default Navbar;
