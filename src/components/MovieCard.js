import { render } from "@testing-library/react";
import React from "react"
import {data} from "../data"
import {addfavourite} from "../action"

  class MovieCard extends React.Component {
      handlefavouriteclick=()=>{
          
          const {movie}=this.props;
    
          this.props.dispatch(addfavourite(movie));
      }
      handleunfavouriteclick=()=>{}
    render(){
        var {movie,isfavourite}=this.props;
       
            return(
                
                <div className="movie-card">
                    <div className="left">
                        <img alt="movie-poster" src={movie.Poster} />
                    </div>
                    <div className="right">
                       <div className="title">
                            {movie.Title}
                       </div>
                       <div className="plot">
                            {movie.Plot}
                       </div>
                       <div className="footer">
                            <div className="rating">
                                {movie.imdbRating}
                            </div>
                            {
                                isfavourite?<button className="unfavourite-btn" onClick={this.handleunfavouriteclick}>UnFavourite</button>
                                :<button className="favourite-btn" onClick={this.handlefavouriteclick}>Favourite</button>
                       
                            }
                            
                            </div>
                    </div>
                </div>
            );
    }

}

export default MovieCard;
