import {data} from "../data"
import Navbar from "./Navbar" 
import MovieCard from "./MovieCard"
import React from "react"
import { render } from "@testing-library/react";
import{add_movies} from "../action/index"
import {showfavouritelist} from "../action/index"
class App extends React.Component{
  componentDidMount(){
    //make an an api call
    //dispatch
    const {store}=this.props;
    store.subscribe(()=>{
      console.log("state is:",this.props.store.getState());
      console.log("UPDATED");
      this.forceUpdate();
    })
     store.dispatch(add_movies(data));
     //console.log("state is:",this.props.store.getState());
  }
  ismoviefavourite=(movie)=>{
    const {favourites}=this.props.store.getState();
    var index=favourites.indexOf(movie);

    if(index!==-1){
      //Found the Movie
      return true;
    }
    return false;
  }
  istabchanged=(val)=>{
     if(val=="false"){
       this.props.store.dispatch(showfavouritelist(val));
     }
     else{
       this.props.store.dispatch(showfavouritelist(val));
     }
  }
  render(){
    var {list}=this.props.store.getState();
    return (
      <div className="App">
        <header className="App-header">
          <Navbar />
          <div className="main">
            <div className="tabs">
                <div className="tab" onClick={()=>this.istabchanged("false")}>Movies</div>
                <div className="tab" onClick={()=>this.istabchanged("true")}>Favourites</div>
            </div>
          </div>
          <div className="list">
                {
                    list.map((movie)=>{
                        return <MovieCard movie={movie} isfavourite={this.ismoviefavourite(movie)} dispatch={this.props.store.dispatch} />
                    })
                }
          </div>
        </header>
      </div>
    );
  }
 
}

export default App;
