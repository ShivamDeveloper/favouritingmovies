
//action types
// import {data} from "../data"
export const ADD_MOVIES="ADD_MOVIES";
export const ADD_FAVOURITE="ADD_FAVOURITE";
export const ADD_TO_FAVOURITE="ADD_TO_FAVOURITE"
// action creators
export function add_movies(movies){
    return {
         type:ADD_MOVIES,
         movies:movies
    }
}
export function showfavouritelist(val){
    return{
        type:ADD_TO_FAVOURITE,
        val
    }
    
}
export function addfavourite(movie){
    return {
         type:ADD_FAVOURITE,
         movie
    }
}